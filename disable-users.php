<?php
/**
 * Plugin Name: Disable Users
 * Description: This plugin stops disabled users logging in.
 * Version:     1.0.0
 * Author:      Aaron Sly
 * Author URI:  http://www.essentialcommerce.co.uk
 *
 * @author     Aaron Sly
 * @version    1.0.0
 * @package    ec_disable_users
 */

final class ec_disable_users {

	/**
	 * Initialize all the things
	 *
	 * @since 1.0.0
	 */
	function __construct() {

		// Actions		
		add_action( 'wp_login',                   array( $this, 'user_login'                  ), 10, 2 );
		add_action( 'manage_users_custom_column', array( $this, 'manage_users_column_content' ), 10, 3 );
		add_action( 'admin_footer-users.php',	  array( $this, 'manage_users_css'            )        );
		
		// Filters
		add_filter( 'login_message',              array( $this, 'user_login_message'          )        );
		add_filter( 'manage_users_columns',       array( $this, 'manage_users_columns'	      )        );
	}
	

	/**
	 * After login check to see if user account is disabled
	 *
	 * @since 1.0.0
	 * @param string $user_login
	 * @param object $user
	 */
	public function user_login( $user_login, $user = null ) {
		
		if ( !$user ) {
			$user = get_user_by('login', $user_login);
		}
		if ( !$user ) {
			// not logged in - not disabled
			return;
		}
		// Get user meta
		$disabled = get_user_meta( $user->ID, 'ec_disable_user', true );
		
		// Is the user logging in disabled?
		if ( $disabled == '1' ) {
			// Clear cookies / log user out
			wp_clear_auth_cookie();

			// Build login URL and then redirect
			$login_url = site_url( $_SERVER['REQUEST_URI'], 'login' );
			$login_url = add_query_arg( 'disabled', '1', $login_url );
			wp_redirect( $login_url );
			exit;
		} elseif ( $user->caps['wpc_client'] ) {
			
			$to = get_field( 'admin_notifications', 'options' );
			$subject = 'Portal Login';
			$body = $user->user_login . ' have just logged into the Customer Portal.';
			$headers = array('Content-Type: text/html; charset=UTF-8');

			wp_mail( $to, $subject, $body, $headers );		
			
		}
	}

	/**
	 * Show a notice to users who try to login and are disabled
	 *
	 * @since 1.0.0
	 * @param string $message
	 * @return string
	 */
	public function user_login_message( $message ) {

		// Show the error message if it seems to be a disabled user
		if ( isset( $_GET['disabled'] ) && $_GET['disabled'] == 1 ) {
			$text = 'Account Disabled <br>Contact <a href="mailto:' . get_field('e-mail', 'option'). '">' . get_field('e-mail', 'option') . '</a> for access.';
			$message =  '<div id="login_error">' . apply_filters( 'ec_disable_users_notice', __( $text, 'ec_disable_users' ) ) . '</div>';

		return $message;
		}
	}

	/**
	 * Add custom disabled column to users list
	 *
	 * @since 1.0.0
	 * @param array $defaults
	 * @return array
	 */
	public function manage_users_columns( $defaults ) {

		$defaults['ec_user_disabled'] = __( 'Disabled', 'ec_disable_users' );
		return $defaults;
	}

	/**
	 * Set content of disabled users column
	 *
	 * @since 1.0.0
	 * @param empty $empty
	 * @param string $column_name
	 * @param int $user_ID
	 * @return string
	 */
	public function manage_users_column_content( $empty, $column_name, $user_ID ) {

		if ( $column_name == 'ec_user_disabled' ) {
			if ( get_the_author_meta( 'ec_disable_user', $user_ID )	== 1 ) {
				return __( 'Disabled', 'ec_disable_users' );
			}
		}
	}

	/**
	 * Specifiy the width of our custom column
	 *
	 * @since 1.0.0
 	 */
	public function manage_users_css() {
		echo '<style type="text/css">.column-ec_user_disabled { width: 60px; }</style>';
	}
}
new ec_disable_users();